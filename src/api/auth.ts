import { Log } from "../util/log";
import { RequestService } from "../util/request";

class Auth {
    class = 'AuthClass';

    constructor() {
    }

    public async login(p: any) {
        return new Promise(async (resolve, reject) => {
            const url = p.domain + '/apiv1/Authenticate';
            const params = '?userName=' + p.username + '&password=' + p.password;
            try {
                const data: any = await RequestService.GET(url + params);
                resolve(data.result);
            } catch (err) {
                Log.error(err, this.class, 'login');
                reject([]);
            }
        });
    }
}

export let AuthClass = new Auth();