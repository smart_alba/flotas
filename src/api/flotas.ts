import * as moment from 'moment';
import { Log } from "../util/log";
import { RequestService } from "../util/request";

class Flotas {
    class = 'FlotasClass';

    constructor() {
    }

    public async getDevices(p: any, pass: string) {
        return new Promise(async (resolve, reject) => {
            const url = 'https://' + p.path + '/apiv1/Get?' + this.getCredentialsParams(p.credentials, pass) + '&typeName=Device&search=' + this.getDateParams();
            try {
                const data: any = await RequestService.GET(url);
                resolve(data.result.map(this.cleanDevice));
            } catch (err) {
                Log.error(err, this.class, 'getDevices');
                reject([]);
            }
        });
    }

    public async getTrips(p: any, pass: string) {
        return new Promise(async (resolve, reject) => {
            const url = 'https://' + p.path + '/apiv1/Get?' + this.getCredentialsParams(p.credentials, pass) + '&typeName=Trip&search=' + this.getDateParams();
            try {
                const data: any = await RequestService.GET(url);
                resolve(data.result.map(this.cleanTrip));
            } catch (err) {
                Log.error(err, this.class, 'getTrips');
                reject([]);
            }
        });
    }

    private getCredentialsParams(credentials: any, password: string) {
        return 'credentials={"database":"' + credentials.database + '","sessionId":"' + credentials.sessionId + '","userName":"' + credentials.userName + '","password":"' + password + '"}';
    }

    private getDateParams() {
        const today = moment().format('YYYY-MM-DD');
        const yesterday = moment().subtract(1, 'days').format('YYYY-MM-DD');
        return '{"fromDate":"' + yesterday + '","toDate":"' + today + '"}';
    }

    private cleanDevice(device: any) {
        const _d = {...device};
        delete _d.deviceFlags;
        _d['deviceFlags_activeFeatures'] = device.deviceFlags.activeFeatures;
        _d['deviceFlags_isActiveTrackingAllowed'] = device.deviceFlags.isActiveTrackingAllowed;
        _d['deviceFlags_isEngineAllowed'] = device.deviceFlags.isEngineAllowed;
        _d['deviceFlags_isGarminAllowed'] = device.deviceFlags.isGarminAllowed;
        _d['deviceFlags_isHOSAllowed'] = device.deviceFlags.isHOSAllowed;
        _d['deviceFlags_isIridiumAllowed'] = device.deviceFlags.isIridiumAllowed;
        _d['deviceFlags_isOdometerAllowed'] = device.deviceFlags.isOdometerAllowed;
        _d['deviceFlags_isTripDetailAllowed'] = device.deviceFlags.isTripDetailAllowed;
        _d['deviceFlags_isUIAllowed'] = device.deviceFlags.isUIAllowed;
        _d['deviceFlags_isVINAllowed'] = device.deviceFlags.isVINAllowed;
        _d['deviceFlags_ratePlans'] = device.deviceFlags.ratePlans;
        delete _d.customFeatures;
        _d['customFeatures_autoHos'] = device.customFeatures && device.customFeatures.autoHos || '';
        delete _d.groups;
        _d['groups_id'] = device.groups[0].id;
        delete _d.devicePlans;
        _d['devicePlans'] = device.devicePlans[0];
        return _d;
    }

    private cleanTrip(trip: any) {
        const _t = {...trip};
        delete _t.stopPoint;
        delete _t.device;
        _t['stopPoint_x'] = trip.stopPoint.x;
        _t['stopPoint_y'] = trip.stopPoint.y;
        _t['device_id'] = trip.device.id;
        return _t;
    }
}

export let FlotasClass = new Flotas();
