import * as dotenv from "dotenv";

import { Log } from "./util/log";
import { AuthClass } from "./api/auth";
import { FileUtil } from "./util/file";
import { FlotasClass } from "./api/flotas";


dotenv.config({path: ".env"});

const config = {
    domain: process.env.DOMAIN,
    username: process.env.USER,
    password: process.env.PASSWORD
};

function getDevices() {
    return new Promise(async (resolve, reject) => {
        const auth: any = await AuthClass.login(config);
        const data: any = await FlotasClass.getDevices(auth, config.password);
        try {
            await FileUtil.saveFile('devices.csv', data);
        } catch (err) {
            Log.error(err, "MAIN", 'Execute Tasks - Save Devices');
        }
        resolve(data);
    });
}

function getTrips() {
    return new Promise(async (resolve, reject) => {
        const auth: any = await AuthClass.login(config);
        const data: any = await FlotasClass.getTrips(auth, config.password);
        try {
            await FileUtil.saveFile('trips.csv', data);
        } catch (err) {
            Log.error(err, "MAIN", 'Execute Tasks - Save Trips');
        }
        resolve(data);
    });
}

function tasks() {
    getDevices().then((data: any) => {
        console.log("Devices: " + data.length);
    }).catch((err: any) => {
        Log.error(err, "MAIN", 'Call Tasks');
    });

    getTrips().then((data: any) => {
        console.log("Trips: " + data.length);
    }).catch((err: any) => {
        Log.error(err, "MAIN", 'Call Tasks');
    });
}

tasks();