import *  as fs from "fs";
import { Log } from "./log";

class Service {
    static class = "FileUtil";

    constructor() {
    }

    public static async saveFile(path: string, data: any) {
        return new Promise(async (resolve, reject) => {
            fs.writeFile(process.env.OUT_PATH + path, Service.convertToCSV(data), 'utf8', function (err) {
                if (err) {
                    Log.error(err.stack, Service.class, 'saveFile');
                    return reject();
                }
                return resolve();
            });
        });
    };

    static parseDuration(interval: string): number {
        const parts = interval.split(":");
        if (parts.length < 3) {
            return 0;
        }
        const horas = parseFloat(parts[0]);
        const minutos = parseFloat(parts[1]);
        const segundos = parseFloat(parts[2]);
        return (horas * 3600 + minutos * 60 + segundos);
    };

    static convertToCSV(objArray: any) {
        const array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        const cabeceras = Object.keys(array[0]).sort();
        let str = '\"' + cabeceras.join('"' + process.env.OUT_SEPARATOR + '"') + '\"\r\n';

        // Agrego funciones para convertir los intervalos en segundos.
        // Es más fácil hacerlo aquí que en NiFi.
        let xforms: Array<[string, { (input: string): number; }]> = [];
        for (const key of cabeceras) {
            const xform = (key.endsWith("Duration")) ? Service.parseDuration : null;
            xforms.push([key, xform]);
        }

        for (let i = 0; i < array.length; i++) {
            let line = '';
            for (let [key, xform] of xforms) {
                if (line != '') line += process.env.OUT_SEPARATOR;
                let val = array[i][key];
                if (val === undefined) {
                    val = "";
                } else if (xform !== null) {
                    val = xform(val);
                }
                line += '"' + val + '"';
            }
            str += line + '\r\n';
        }
        return str;
    }
}


export let FileUtil = Service;
